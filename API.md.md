##Register wallet
```
#!php
$wallet->register($email);
```
* $email - is user email, this variable is optional

returns false if request failed or an Object containing wallet credentials if it succeeded

##Authenticate

```
#!php
$wallet->setAuth($key, $secret);
```

* $key - is wallet's api key
* $secret - wallet secret token, its used to sign transactions

##Send
```
#!php
$wallet->send($amount, $address);
```
* $amount - is amount of GHC to send
* $address - is destination GHC address

returns txid if request succeeded or false otherwise

##Balance
```
#!php
$wallet->balance();
```
returns user balance or false if request failed

## Wallet Transactions
```
#!php
$wallet->transactions();
```
returns an array of wallet transactions

##Generate address
```
#!php
$wallet->generate();
```
returns new generated GHC address or false if request failed

##Create Payment Template
```
#!php
$wallet->addPaymentTemplate($amount, $title, $callback, $callbackKey);
```
* $amount - GHC amount, if amount is 0 anything greater than 0 will consider checkout complete
* $title - alias that helps to identify template, it is optional
* $callback - is checkout callback url, it is optional
* $callbackKey - used to sign callback request, if no callback key is provided request will not be signed

Returns an object representing payment template or false if request failed

##Checkout
```
#!php
$wallet->checkout($payment_id, $text);
```
* $payment_id - is payment template ID created earlier with addPaymentTemplate
* $text - is a custom text that will be passed back to callback once checkout will be complete, this variable can be used to serialize various data types that are needed in callback script

##Check checkout status
```
#!php
$walet->check($id, $payment_id);
```
* $id - is checkout ID, retrieved from $wallet->checkout
* $payment_id - is payment template ID, that was retrieved from $wallet->checkout

returns chekout state object or false

## List Payment Templates
```
#!php
$wallet->paymentTemplates();
```
returns an array of objects representing payment templates or false if it failed

## List checkouts
```
#!php
$wallet->invoices($payment_id)
```
* $payment_id - is payment template ID

returns an array of checkouts or false

## Delete Payment template

```
#!php
$wallet->removePaymentTemplate($id);
```
* $id - payment template ID
returns true if request succeeded or false otherwise

